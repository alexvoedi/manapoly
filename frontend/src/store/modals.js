export default {
	state: {
		buyPropertyModal: {
			show: false,
		},
		payRentModal: {
			show: false,
		},
	},

	mutations: {
		buyPropertyModal(state, value) {
			state.buyPropertyModal = value
		},
	},

	actions: {
		SOCKET_offerProperty({ commit }, payload) {
			commit('buyPropertyModal', {
				show: true,
				property: payload
			})
		},

		SOCKET_payRent({ commit }) {
			commit('payRentModal', true)
		},
	},
}
