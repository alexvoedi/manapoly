import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import modals from './modals'

export default new Vuex.Store({
	state: {
		game: null,
		playerId: '',
	},

	mutations: {
		SOCKET_gameState(state, game) {
			state.game = game
		},

		SOCKET_playerId(state, playerId) {
			state.playerId = playerId
		},

		SOCKET_chanceCard(state, text) {
			console.log(text)
		},

		SOCKET_communityChestCard(state, text) {
			console.log(text)
		},
	},

	actions: {
		SOCKET_connect() {
			this._vm.$socket.emit(
				'joinGame',
				`${Math.floor(1000 + Math.random() * 9000)}`,
			)
		},

		buyProperty({ commit }) {
			this._vm.$socket.emit('buyProperty')
			commit('buyPropertyModal', { show: false })
		},

		buyHouse({}, fieldId) {
			this._vm.$socket.emit('buyHouse', fieldId)
		},

		rollDice() {
			this._vm.$socket.emit('rollDice')
		},

		endTurn() {
			this._vm.$socket.emit('endTurn')
		},
	},

	getters: {
		getFieldById: (state) => (id) => {
			return state.game.fields.find((field) => field.id === id)
		},
	},

	modules: {
		modals,
	},
})
