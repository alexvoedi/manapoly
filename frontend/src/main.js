import Vue from 'vue'
import VueSocketIO from 'vue-socket.io'

import './assets/styles/index.css'

import App from './App.vue'

import router from './router'
import store from './store'

Vue.config.productionTip = false

Vue.use(new VueSocketIO({
  debug: true,
  connection: 'http://localhost:8000',
  vuex: {
    store,
    actionPrefix: 'SOCKET_',
    mutationPrefix: 'SOCKET_',
  },
}))

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
