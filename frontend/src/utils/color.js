export function isLight(c) {
	var c = c.substring(1)
	var rgb = parseInt(c, 16)
	var r = (rgb >> 16) & 0xff
	var g = (rgb >> 8) & 0xff
	var b = (rgb >> 0) & 0xff

	var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b

	return luma > 200
}
