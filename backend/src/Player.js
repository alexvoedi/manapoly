import playerStates from './player-states'

export class Player {
	constructor(id, name, color) {
		this.id = id
		this.name = name
		this.color = color

		this.properties = []
		this.currentField = null
		this.inJail = false

		this.money = 30000
		this.state = playerStates.READY
	}

	moveBy(value, fields) {
		let nextFieldId = this.currentField.id + value

		if (nextFieldId > 39) {
			this.money += 4000
			nextFieldId %= 40
		}

		this.currentField = fields.find((field) => field.id === nextFieldId)
	}

	pay(value) {
		let moneyAfter = this.money - value

		console.log(moneyAfter)

		if (moneyAfter >= 0) {
			this.money = moneyAfter
			return true
		} else {
			//todo
			return false
		}
	}

	earn(value) {
		this.money += value
	}

	buyProperty(property = this.currentField) {
		this.pay(property.price)
		this.properties.push(property)
		this.properties.sort((a, b) => a.id - b.id)
		property.sold = true
	}

	buyHouse(fieldId, fields) {
		let property = this.properties.find((property) => property.id === fieldId)

		// check if property is street
		if (property.type !== 'street') return

		// check if player has whole street group
		if (!property.hasFullGroup(this, fields)) return

		// check if each property has enough houses
		let group = property.getGroupFields(property.group, fields)
		if (group.some((groupProperty) => groupProperty.houses < property.houses)) return

		// check if there are not too many houses
		if (property.houses >= 5) return

		if (this.pay(property.group.housePrice)) {
			property.houses++
		}
	}

	sellHouse(fieldId, fields) {
		let property = this.properties.find((property) => property.id === fieldId)

		// check if property is street
		if (property.type !== 'street') return

		// check if each property has enough houses
		let group = property.getGroupFields(property.group, fields)
		if (group.some((groupProperty) => groupProperty.houses > property.houses)) return

		// check if there are not too many houses
		if (property.houses == 0) return

		property.houses--
		this.earn(property.group.housePrice / 2)
	}

	serialize() {
		return {
			id: this.id,
			name: this.name,
			color: this.color,
			properties: this.properties,
			currentField: this.currentField,
			inJail: this.inJail,
			money: this.money,
		}
	}
}
