import { Player } from './Player'
import { Chance } from './fields/Chance'
import { CommunityChest } from './fields/CommunityChest'
import { FreeParking } from './fields/FreeParking'
import { Go } from './fields/Go'
import { GoToJail } from './fields/GoToJail'
import { Group } from './fields/Group'
import { InJail } from './fields/InJail'
import { Station } from './fields/Station'
import { Street } from './fields/Street'
import { Tax } from './fields/Tax'
import { Utility } from './fields/Utility'

import fields from './fields'
import gameStates from './game-states'

import { Property } from './fields/Property'

export class Game {
	constructor(options) {
		this.options = options
		this.state = gameStates.WAITING_PLAYERS_JOIN
		this.players = []
		this.currentPlayer = null
		this.dice = { firstDie: null, secondDie: null }
		this.fields = []

		this.initFields()
	}

	initFields() {
		fields.forEach((field) => {
			if (field.type === 'go') this.initGo(field)
			else if (field.type === 'property') this.initProperties(field.properties)
			else if (field.type === 'community-chest') this.initCommunityChests(field.fields)
			else if (field.type === 'tax') this.initTaxes(field.fields)
			else if (field.type === 'chance') this.initChances(field.fields)
			else if (field.type === 'in-jail') this.initInJail(field)
			else if (field.type === 'free-parking') this.initFreeParking(field)
			else if (field.type === 'go-to-jail') this.initGoToJail(field)
		})
	}

	initGo(go) {
		this.fields.push(new Go(go.id, 'go', go.name))
	}

	initProperties(properties) {
		properties.forEach((property) => {
			if (property.type === 'street') this.initGroups(property.groups)
			else if (property.type === 'station') this.initStations(property.fields)
			else if (property.type === 'utility') this.initUtilities(property.fields)
		})
	}

	initGroups(groups) {
		groups.forEach((group) => {
			let groupObj = new Group(group.color, group.housePrice)
			this.initStreets(group.streets, groupObj)
		})
	}

	initStreets(streets, group) {
		streets.forEach((street) => {
			let streetObj = new Street(street.id, 'street', street.name, street.price, street.rent, group)
			this.fields.push(streetObj)
		})
	}

	initStations(stations) {
		stations.forEach((station) => {
			let stationObj = new Station(station.id, 'station', station.name, station.price)
			this.fields.push(stationObj)
		})
	}

	initUtilities(utilities) {
		utilities.forEach((utility) => {
			this.fields.push(new Utility(utility.id, 'utility', utility.name, utility.price))
		})
	}

	initCommunityChests(communityChests) {
		communityChests.forEach((communityChest) => {
			this.fields.push(new CommunityChest(communityChest.id, 'community-chest', communityChest.name))
		})
	}

	initTaxes(taxes) {
		taxes.forEach((tax) => {
			this.fields.push(new Tax(tax.id, 'tax', tax.name, tax.price))
		})
	}

	initChances(chances) {
		chances.forEach((chance) => {
			this.fields.push(new Chance(chance.id, 'chance', chance.name))
		})
	}

	initInJail(inJail) {
		this.fields.push(new InJail(inJail.id, 'in-jail', inJail.name))
	}

	initFreeParking(freeParking) {
		this.fields.push(new FreeParking(freeParking.id, 'free-parking', freeParking.name))
	}

	initGoToJail(goToJail) {
		this.fields.push(new GoToJail(goToJail.id, 'go-to-jail', goToJail.name))
	}

	start() {
		if (this.state !== gameStates.WAITING_PLAYERS_JOIN) return

		this.nextPlayer()
	}

	nextPlayer() {
		this.state = gameStates.WAITING_PLAYER_ROLL

		if (this.currentPlayer && this.dice.firstDie !== this.dice.secondDie) {
			let nextPlayerIndex = this.players.indexOf(this.currentPlayer) + 1
			let nextPlayer = this.players[nextPlayerIndex % this.players.length]
			this.currentPlayer = nextPlayer
		} else {
			let randomIndex = Math.floor(Math.random() * this.players.length)
			this.currentPlayer = this.players[randomIndex]
		}
	}

	addPlayer(id, name) {
		let player = new Player(
			id,
			name,
			'#' + ((Math.random() * 0xffffff) << 0).toString(16),
		)
		player.currentField = this.fields.find((field) => field.id === 0)
		this.players.push(player)

		// for debugging
		if (this.players.length === 1) {
			let properties = this.fields.filter((field) => field instanceof Property)
			properties.forEach((property) => property.sold = true)
			player.properties.push(...properties)
		}
	}

	rollDice() {
		this.dice = {
			firstDie: Math.floor(Math.random() * 6 + 1),
			secondDie: Math.floor(Math.random() * 6 + 1),
		}

		let sum = this.dice.firstDie + this.dice.secondDie

		this.currentPlayer.moveBy(sum, this.fields)
		this.currentPlayer.currentField.onEnter(this.currentPlayer, this)

		this.state = gameStates.WAITING_PLAYER_END_TURN
	}
}
