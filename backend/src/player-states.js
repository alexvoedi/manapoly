export default {
	READY: {
		name: 'READY',
	},
	DOING_PAYMENT: {
		name: 'DOING_PAYMENT',
	},
	IN_JAIL: {
		name: 'IN_JAIL',
	},
	WAITING_ACKNOWLEDGE: {
		name: 'WAITING_ACKNOWLEDGE',
	},
}
