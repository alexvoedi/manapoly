export class Field {
	constructor(id, type, name) {
		this.id = id
		this.type = type
		this.name = name
	}

	onEnter() {}

	serialize() {
		return {
			id: this.id,
			type: this.type,
			name: this.name,
		}
	}
}
