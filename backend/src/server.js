import Server from 'socket.io'

import { Game } from './Game'

let io = Server(8000)

let game = null

io.on('connection', (socket) => onConnect(socket))

function onConnect(socket) {
	if (!game) {
		game = new Game({
			maxPlayers: 1,
		})
	}

	addEvents(socket)
}

function addEvents(socket) {
	socket.on('joinGame', (name) => onPlayerJoin(socket, name))
	socket.on('rollDice', () => onRollDice())
	socket.on('buyProperty', () => onBuyProperty())
	socket.on('buyHouse', (fieldId) => onBuyHouse(socket.id, fieldId))
	socket.on('endTurn', () => onEndTurn())
	socket.on('disconnect', (socket) => onDisconnect(socket))
}

function onDisconnect(socket) {
	if (!game) return

	game.players = game.players.filter((player) => player.id === socket.id)

	if (game.players.length < 1) {
		game = null
	}

	io.to('game-room').emit('gameState', game)
}

function onPlayerJoin(socket, name) {
	socket.join('game-room')

	if (game.options.maxPlayers > io.sockets.clients.length) {
		return socket.disconnect(true)
	}

	game.addPlayer(socket.id, name)

	if (game.options.maxPlayers === io.sockets.clients.length) {
		game.start()
	}

	socket.emit('playerId', socket.id)
	io.to('game-room').emit('gameState', game)
}

function onRollDice() {
	game.rollDice()

	io.to('game-room').emit('gameState', game)
}

function onBuyProperty() {
	game.currentPlayer.buyProperty()

	io.to('game-room').emit('gameState', game)
}

function onBuyHouse(playerId, fieldId) {
	let player = game.players.find((player) => player.id === playerId)

	player.buyHouse(fieldId, game.fields)

	io.to('game-room').emit('gameState', game)
}

function onEndTurn() {
	game.nextPlayer()

	io.to('game-room').emit('gameState', game)
}

export default io
