export default [
	{
		id: 0,
		type: 'go',
		name: 'Los',
	},

	{
		type: 'property',
		properties: [
			{
				type: 'street',
				groups: [
					{
						color: '#7d4314',
						housePrice: 1000,
						streets: [
							{
								id: 1,
								name: 'Badstraße',
								price: 1200,
								rent: [40, 200, 600, 1800, 3200, 5000],
							},
							{
								id: 3,
								name: 'Turmstraße',
								price: 1200,
								rent: [80, 400, 1200, 3600, 6400, 9000],
							},
						],
					},
					{
						color: '#80ccff',
						housePrice: 1000,
						streets: [
							{
								id: 6,
								name: 'Chausseestraße',
								price: 2000,
								rent: [120, 600, 1800, 5400, 8000, 11000],
							},
							{
								id: 8,
								name: 'Elisenstraße',
								price: 2000,
								rent: [120, 600, 1800, 5400, 8000, 11000],
							},
							{
								id: 9,
								name: 'Poststraße',
								price: 2400,
								rent: [160, 800, 2000, 6000, 9000, 12000],
							},
						],
					},
					{
						color: '#cc44cc',
						housePrice: 2000,
						streets: [
							{
								id: 11,
								name: 'Seestraße',
								price: 2800,
								rent: [200, 1000, 3000, 9000, 12500, 15000],
							},
							{
								id: 13,
								name: 'Hafenstraße',
								price: 2800,
								rent: [200, 1000, 3000, 9000, 12500, 15000],
							},
							{
								id: 14,
								name: 'Neue Straße',
								price: 3200,
								rent: [240, 1200, 3600, 10000, 14000, 18000],
							},
						],
					},
					{
						color: '#ff8000',
						housePrice: 2000,
						streets: [
							{
								id: 16,
								name: 'Münchener Straße',
								price: 3600,
								rent: [280, 1400, 4000, 11000, 15000, 19000],
							},
							{
								id: 18,
								name: 'Wiener Straße',
								price: 3600,
								rent: [280, 1400, 4000, 11000, 15000, 19000],
							},
							{
								id: 19,
								name: 'Berliner Straße',
								price: 4000,
								rent: [320, 1600, 4400, 12000, 16000, 20000],
							},
						],
					},
					{
						color: '#ff0000',
						housePrice: 3000,
						streets: [
							{
								id: 21,
								name: 'Theaterstraße',
								price: 4400,
								rent: [360, 1800, 5000, 14000, 17500, 21000],
							},
							{
								id: 23,
								name: 'Museumstraße',
								price: 4400,
								rent: [360, 1800, 5000, 14000, 17500, 21000],
							},
							{
								id: 24,
								name: 'Opernplatz',
								price: 4800,
								rent: [400, 2000, 6000, 15000, 18500, 22000],
							},
						],
					},
					{
						color: '#ffff00',
						housePrice: 3000,
						streets: [
							{
								id: 26,
								name: 'Lessingstraße',
								price: 5200,
								rent: [440, 2200, 6600, 16000, 19500, 23000],
							},
							{
								id: 27,
								name: 'Schillerstraße',
								price: 5200,
								rent: [440, 2200, 6600, 16000, 19500, 23000],
							},
							{
								id: 29,
								name: 'Goethestraße',
								price: 5600,
								rent: [480, 2400, 7200, 17000, 20500, 24000],
							},
						],
					},
					{
						color: '#338033',
						housePrice: 4000,
						streets: [
							{
								id: 31,
								name: 'Rathhausplatz',
								price: 6000,
								rent: [520, 2600, 7800, 18000, 22000, 25500],
							},
							{
								id: 32,
								name: 'Hauptstraße',
								price: 6000,
								rent: [520, 2600, 7800, 18000, 22000, 25500],
							},
							{
								id: 34,
								name: 'Bahnhofstraße',
								price: 6400,
								rent: [560, 3000, 9000, 20000, 24000, 28000],
							},
						],
					},
					{
						color: '#2f347a',
						housePrice: 4000,
						streets: [
							{
								id: 37,
								name: 'Parkstraße',
								price: 7000,
								rent: [700, 3500, 10000, 22000, 26000, 30000],
							},
							{
								id: 39,
								name: 'Schlossallee',
								price: 8000,
								rent: [1000, 4000, 12000, 28000, 34000, 40000],
							},
						],
					},
				],
			},
			{
				type: 'station',
				fields: [
					{
						id: 5,
						name: 'Südbahnhof',
						price: 4000,
					},
					{
						id: 15,
						name: 'Westbahnhof',
						price: 4000,
					},
					{
						id: 25,
						name: 'Nordbahnhof',
						price: 4000,
					},
					{
						id: 35,
						name: 'Hauptbahnhof',
						price: 4000,
					},
				],
			},
			{
				type: 'utility',
				fields: [
					{
						id: 12,
						name: 'Elektrizitätswerk',
						price: 3000,
					},
					{
						id: 28,
						name: 'Wasserwerk',
						price: 3000,
					},
				],
			},
		],
	},

	{
		type: 'community-chest',
		fields: [
			{
				id: 2,
				name: 'Gemeinschaftsfeld',
			},
			{
				id: 17,
				name: 'Gemeinschaftsfeld',
			},
			{
				id: 33,
				name: 'Gemeinschaftsfeld',
			},
		],
	},

	{
		type: 'tax',
		fields: [
			{
				id: 4,
				name: 'Einkommensteuer',
				price: 4000,
			},
			{
				id: 38,
				name: 'Zusatzsteuer',
				price: 2000,
			},
		],
	},

	{
		type: 'chance',
		fields: [
			{
				id: 7,
				name: 'Ereignisfeld',
			},
			{
				id: 22,
				name: 'Ereignisfeld',
			},
			{
				id: 36,
				name: 'Ereignisfeld',
			},
		],
	},

	{
		id: 10,
		type: 'in-jail',
		name: 'Im Gefängnis / Nur zu Besuch',
	},

	{
		id: 20,
		type: 'free-parking',
		name: 'Frei Parken',
	},

	{
		id: 30,
		type: 'go-to-jail',
		name: 'Gehen Sie in das Gefängnis',
	},
]
