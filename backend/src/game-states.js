export default {
	WAITING_PLAYERS_JOIN: {
		name: 'WAITING_PLAYERS_JOIN',
	},
	WAITING_PLAYER_ROLL: {
		name: 'WAITING_PLAYER_ROLL',
	},
	WAITING_PLAYER_END_TURN: {
		name: 'WAITING_PLAYER_END_TURN',
	},
}
