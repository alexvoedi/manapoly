export default [
	{
		text:
			'Rücke vor bis zur Seestraße. Wenn du über Los kommst, ziehe DM 4000 ein.',
		event: (player, game) => {},
	},
	{
		text:
			'Du hast in einem Kreuzworträtselwettbewerb gewonnen. Ziehe DM 2000 ein.',
		event: (player, game) => {
			player.earch(2000)
		},
	},
	{
		text: 'Miete und Anleihezinsen werden fällig. Die Bank zahlt dir DM 3000.',
		event: (player, game) => {
			player.earch(3000)
		},
	},
	{
		text: 'Du kommst aus dem Gefängnis frei. Diese Karte musst du behalten bis du sie verwendest oder verkaufst.',
		event: (player, game) => {},
	},
	{
		text: 'Rücke vor bis auf Los.',
		event: (player, game) => {},
	},
	{
		text: 'Die Bank zahlt dir eine Dividende von DM 1000.',
		event: (player, game) => {
			player.earn(1000)
		},
	},
	{
		text: 'Rücke vor bis zur Schlossallee.',
		event: (player, game) => {},
	},
	{
		text:
			'Rücke vor bis zum Opernplatz. Wenn du über Los kommst, ziehe DM 4000 ein.',
		event: (player, game) => {},
	},
	{
		text: 'Gehe 3 Felder zurück.',
		event: (player, game) => {},
	},
	{
		text:
			'Lasse alle deine Häuser renovieren: Zahle an für jedes Haus DM 500, zahle für jedes Hotel DM 2000.',
		event: (player, game) => {},
	},
	{
		text:
			'Du wirst zu Straßenausbesserungsarbeiten herangezogen. Zahle: DM 800 je Haus, DM 2300 je Hotel.',
		event: (player, game) => {},
	},
	{
		text:
			'Gehe in das Gefängnis. Begebe dich direkt dorthin. Gehe nicht über Los und ziehe nicht DM 4000 ein.',
		event: (player, game) => {},
	},
	{
		text: 'Betrunken im Dienst. Strafe: DM 400.',
		event: (player, game) => {
			player.pay(400)
		},
	},
	{
		text: 'Zahle Schulgeld: DM 3000.',
		event: (player, game) => {
			player.pay(3000)
		},
	},
	{
		text: 'Strafe für zu schnelles Fahren: DM 300.',
		event: (player, game) => {
			player.pay(300)
		},
	},
	{
		text: 'Mache einen Ausflug zum Südbahnhof. Wenn du über Los kommst, ziehe DM 4000 ein.',
		event: (player, game) => {},
	},
]
