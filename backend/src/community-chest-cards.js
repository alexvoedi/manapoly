export default [
	{
		text:
			'Zahle eine Strafe von DM 200 oder nimm eine Ereigniskarte.',
		event: (player, game) => {},
	},
	{
		text:
			'Die Jahresrente wird fällig: Ziehe DM 2000 ein.',
		event: (player, game) => {
			player.earn(2000)
		},
	},
	{
		text:
			'Du kommst aus dem Gefängnis frei. Diese Karte musst du behalten bis du sie verwendest oder verkaufst.',
		event: (player, game) => {

		},
	},
	{
		text:
			'Du hast den 2. Preis in einer Schönheitskonkurrenz gewonnen. Ziehe DM 200 ein.',
		event: (player, game) => {
			player.earn(200)
		},
	},
	{
		text:
			'Aus Lagerverkäufen erhältst du DM 1000.',
		event: (player, game) => {
			player.earn(1000)
		},
	},
	{
		text:
			'Arztkosten: Zahle DM 1000.',
		event: (player, game) => {
			player.pay(1000)
		},
	},
	{
		text:
			'Rücke vor bis auf Los.',
		event: (player, game) => {},
	},
	{
		text:
			'Du erhältst auf Vorzugsaktien 7 % Dividende. Ziehe DM 500 ein.',
		event: (player, game) => {
			player.earn(500)
		},
	},
	{
		text:
			'Du erbst DM 2000.',
		event: (player, game) => {
			player.earn(2000)
		},
	},
	{
		text:
			'Gehe zurück zur Badstraße.',
		event: (player, game) => {},
	},
	{
		text:
			'Gehe in das Gefängnis. Begebe dich direkt dorthin. Gehe nicht über los und ziehe nicht DM 4000 ein.',
		event: (player, game) => {},
	},
	{
		text:
			'Bankirrtum zu deinen Gunsten. Ziehe DM 4000 ein.',
		event: (player, game) => {
			player.earn(4000)
		},
	},
	{
		text:
			'Zahle an das Krankenhaus DM 2000.',
		event: (player, game) => {
			player.pay(2000)
		},
	},
	{
		text:
			'Es ist dein Geburtstag. Ziehe von jedem Spieler DM 1000 ein.',
		event: (player, game) => {
			game.players.forEach((payer) => {
				if (payer.id !== player.id) {
					payer.pay(1000)
					player.earn(1000)
				}
			})
		},
	},
	{
		text:
			'Zahle deine Versicherungssumme: DM 1000.',
		event: (player, game) => {
			player.pay(1000)
		},
	},
	{
		text:
			'Einkommensteuerrückzahlung: Ziehe DM 400 ein.',
		event: (player, game) => {
			player.earn(400)
		},
	},
]
