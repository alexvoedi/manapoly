import { Field } from '../Field'

export class Tax extends Field {
	constructor(id, type, name, price) {
		super(id, type, name)

		this.price = price
	}

	onEnter(player) {
		player.pay(this.price)
	}
}
