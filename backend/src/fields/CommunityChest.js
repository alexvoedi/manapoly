import { Field } from '../Field'

import io from '../server'
import communityChestCards from '../community-chest-cards'

export class CommunityChest extends Field {
	constructor(id, type, name) {
		super(id, type, name)
	}

	onEnter(player, game) {
		let card = this.drawCard()

		card.event(player, game)

		io.to(player.id).emit('communityChestCard', card.text)
	}

	drawCard() {
		let card = communityChestCards.pop()

		communityChestCards.unshift(card)

		return card
	}
}
