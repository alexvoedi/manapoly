import { Property } from './Property'

import io from '../server'

export class Station extends Property {
	constructor(id, type, name, price) {
		super(id, type, name, price)
	}

	payRent(visitor, owner) {
		let rent = this.calculateRent(owner)

		visitor.pay(rent)
		owner.earn(rent)

		io.to(visitor.id).emit('payRent', owner)
		io.to(owner.id).emit('earnRent', visitor)
	}

	calculateRent(owner) {
		let ownedStations = owner.properties.filter((property) => property.type === 'station').length

		if (ownedStations === 1) {
			return 500
		} else if (ownedStations === 2) {
			return 1000
		} else if (ownedStations === 3) {
			return 2000
		} else if (ownedStations === 4) {
			return 4000
		} else {
			return 0
		}
	}
}
