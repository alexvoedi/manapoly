import { Property } from './Property'

import io from '../server'

export class Utility extends Property {
	constructor(id, type, name, price) {
		super(id, type, name, price)
	}

	payRent(visitor, owner, game) {
		let rent = (game.dice.firstDie + game.dice.secondDie) * this.factor(owner)

		visitor.pay(rent)
		owner.earn(rent)

		io.to(visitor.id).emit('payRent', owner)
		io.to(owner.id).emit('earnRent', visitor)
	}

	factor(owner) {
		let ownedUtilities = owner.properties.filter((property) => property.type === 'utility').length

		if (ownedUtilities === 1) {
			return 80
		} else if (ownedUtilities === 2) {
			return 200
		} else {
			return 0
		}
	}
}
