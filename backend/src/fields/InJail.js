import { Field } from '../Field'

export class InJail extends Field {
	constructor(id, type, name) {
		super(id, type, name)

		this.players = []
	}

	addPlayer(player) {
		player.inJail = true

		this.players.push({
			player,
			rounds: 0,
		})
	}

	serialize() {
		return {
			id: this.id,
			type: this.type,
			name: this.name,
			players: this.players,
		}
	}
}
