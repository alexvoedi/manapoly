import { Field } from '../Field'

import io from '../server'

export class Property extends Field {
	constructor(id, type, name, price) {
		super(id, type, name)

		this.price = price
		this.sold = false
	}

	onEnter(player, game) {
		if (this.sold) {
			let owner = this.getOwner(game)

			if (owner.id === player.id) return

			this.payRent(player, owner, game)
		} else {
			this.offerProperty(player)
		}
	}

	offerProperty(player) {
		io.to(player.id).emit('offerProperty', this)
	}

	getOwner(game) {
		return game.players.find((player) => {
			return player.properties.some((property) => property.id === this.id)}
		)
	}
}
