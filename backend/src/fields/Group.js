export class Group {
	constructor(color, housePrice) {
		this.color = color
		this.housePrice = housePrice
	}

	serialize() {
		return {
			color: this.color,
			housePrice: this.housePrice,
		}
	}
}
