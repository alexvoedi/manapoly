import { Property } from './Property'

import io from '../server'

export class Street extends Property {
	constructor(id, type, name, price, rent, group) {
		super(id, type, name, price)

		this.rent = rent
		this.group = group
		this.houses = 0
	}

	payRent(visitor, owner, game) {
		let rent = this.rent[this.houses]

		if (this.houses === 0 && this.hasFullGroup(owner, game.fields)) {
			rent *= 2
		}

		visitor.pay(rent)
		owner.earn(rent)

		io.to(visitor.id).emit('payRent', owner)
		io.to(owner.id).emit('earnRent', visitor)
	}

	hasFullGroup(owner, fields) {
		let ownedGroupProperties = owner.properties.filter((field) => field.group === this.group)
		let existingGroupProperties = this.getGroupFields(this.group, fields)
		return ownedGroupProperties.length === existingGroupProperties.length
	}

	getGroupFields(group, fields) {
		return fields.filter((field) => field.group === group)
	}
}
