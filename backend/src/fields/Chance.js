import { Field } from '../Field'

import io from '../server'
import chanceCards from '../chance-cards'

export class Chance extends Field {
	constructor(id, type, name) {
		super(id, type, name)
	}

	onEnter(player, game) {
		let card = this.drawCard()

		card.event(player, game)

		io.to(player.id).emit('chanceCard', card.text)
	}

	drawCard() {
		let card = chanceCards.pop()

		chanceCards.unshift(card)

		return card
	}
}
